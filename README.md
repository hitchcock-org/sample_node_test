Node.js Sample!
=================

A simple Node.js application with tests.

Uses Grunt to run tests against an Express server, then generates reports with Xunit and Istanbul.

This sample is built for Shippable, a docker based continuous integration and deployment platform.
[![Run Status](https://apibeta.shippable.com/projects/56cee5efc77dae78a8ea1ea2/badge?branch=master)](https://beta.shippable.com/projects/56cee5efc77dae78a8ea1ea2)
